<?php

use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\HelloController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\BookSubjectController;
use App\Http\Controllers\BookStorageFloorController;
use App\Http\Controllers\BookRentController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\FrontController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route menuju halaman depan website. Halaman depan berisi daftar buku perpustakaan, dan bisa diakses tanpa perlu melakukan login.
Route::get('/', [FrontController::class, 'front'])->name('/');

// Route menuju dashboard. Dashboard berisi statistik singkat seperti jumlah buku terdaftar, jumlah anggota yang perlu diaktivasi, jumlah peminjaman buku aktif, dan sebagainya.
Route::get('home', [HomeController::class, 'index'])->name('home');

Route::resource('employee', EmployeeController::class)->only(['index', 'store', 'update', 'destroy']);

// Route menuju antarmuka untuk CRUD buku. Link untuk route ini hanya bisa dilihat oleh user yang terdaftar sebagai admin.
Route::resource('book', BookController::class)->only(['index', 'store', 'update', 'destroy']);

// Route menuju antarmuka untuk CRUD subujek/kategori buku. Link untuk route ini hanya bisa dilihat oleh user yang terdaftar sebagai admin.
Route::resource('book-subject', BookSubjectController::class)->only(['index', 'store', 'update', 'destroy']);

// Route menuju antarmuka untuk CRUD data lantai penyimpanan buku. Link untuk route ini hanya bisa dilihat oleh user yang terdaftar sebagai admin.
Route::resource('book-storage-floor', BookStorageFloorController::class)->only(['index', 'store', 'update', 'destroy']);

// Route menuju antarmuka untuk CRUD peminjaman buku. Link untuk route ini hanya bisa dilihat oleh user yang terdaftar sebagai admin.
Route::put('/book-rent/confirmRent/{id}', [BookRentController::class, 'confirmRent'])->name('book-rent.confirmRent');
Route::put('/book-rent/confirmFinishedRent/{id}', [BookRentController::class, 'confirmFinishedRent'])->name('book-rent.confirmFinishedRent');
Route::put('/book-rent/confirm/{photo}', [BookRentController::class, 'confirm'])->name('confirm');;
Route::resource('book-rent', BookRentController::class)->only(['index', 'store', 'update', 'destroy']);

// Route menuju antarmuka untuk CRUD keanggotaan perpustakaan. Link untuk route ini hanya bisa dilihat oleh user yang terdaftar sebagai admin.
Route::resource('user', UserController::class)->only(['index', 'store', 'update', 'destroy','activate']);

// Route untuk melakukan penambahan-pengurangan item buku untuk dipinjam. Route ini dipakai di halaman depan.
Route::post('/', [FrontController::class, 'storeNew'])->name('storeNew');
Route::post('/addBookItem', [FrontController::class, 'storeNewBookItem'])->name('storeNewBookItem');
Route::delete('/destroyBookItem/{id}', [FrontController::class, 'destroyBookItem'])->name('destroyBookItem');


Route::get('showRentedBooks/{id}', [BookRentController::class, 'showRentedBooks'])->name('showRentedBooks');


require __DIR__.'/auth.php';
