<?php

namespace App\Http\Controllers;

use App\Models\BookSubject;
use Illuminate\Http\Request;
use Inertia\Inertia;

class BookSubjectController extends Controller
{
        // Fungsi untuk memastikan bahwa route yang ditangani oleh kontroller ini hanya bisa diakses oleh user yang sudah login.
        public function __construct()
    {
        $this->middleware('auth');
    }

    // Fungsi untuk menampilkan daftar subjek/kategori buku di tampilan admin.
    public function index(Request $request)
    {
        $data = BookSubject::when($request->sort_by, function ($query, $value) {
                $query->orderBy($value, request('order_by', 'asc'));
            })
            ->when(!isset($request->sort_by), function ($query) {
                $query->latest();
            })
            ->when($request->search, function ($query, $value) {
                $query->where('name', 'LIKE', '%'.$value.'%');
            })
            ->paginate($request->page_size ?? 10);
        return Inertia::render('book-subject/index', [
            'items' => $data,
        ]);
    }

    // Fungsi untuk menambahkan subjek/kategori buku baru.
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'name'         => 'required|string',
            'description'   => 'required|string',
        ]);
        BookSubject::create($data);
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Subjek buku baru berhasil dibuat!',
        ]);
    }

    // Fungsi untuk mengupdate subjek/kategori buku.
    public function update(BookSubject $booksubject, Request $request, $id)
    {
        $data = $this->validate($request, [
            'name'         => 'required|string',
            'description'   => 'required|string',
        ]);
        $booksubject->find($id)->update($data);
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Subjek buku berhasil di-edit!',
        ]);
    }

    // Fungsi untuk menghapus subjek/kategori buku.
    public function destroy(BookSubject $booksubject, $id)
    {
        $booksubject->find($id)->delete();
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Subjek buku berhasil dihapus!',
        ]);
    }
}
