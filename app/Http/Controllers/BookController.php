<?php

namespace App\Http\Controllers;

use App\Models\BookStorageFloor;
use App\Models\BookSubject;
use App\Models\Book;
use Illuminate\Http\Request;
use Inertia\Inertia;
 
class BookController extends Controller
{

        // Fungsi untuk memastikan bahwa route yang ditangani oleh kontroller ini hanya bisa diakses oleh user yang sudah login.
        public function __construct()
    {
        $this->middleware('auth');
    }

    // Fungsi untuk menampilkan daftar buku di tampilan admin.
    public function index(Request $request)
    {
        $bookSubjectList = BookSubject::get();
        $bookStorageFloorList = BookStorageFloor::get();
        $data = Book::with('floor')->with('subject')->when($request->sort_by, function ($query, $value) {
                $query->orderBy($value, request('order_by', 'asc'));
            })
            ->when(!isset($request->sort_by), function ($query) {
                $query->latest();
            })
            ->when($request->search, function ($query, $value) {
                $query->where('title', 'LIKE', '%'.$value.'%');
            })
            ->paginate($request->page_size ?? 10);
        return Inertia::render('book/index', [
            'items' => $data,
            'bookSubjectList' => $bookSubjectList,
            'bookStorageFloorList' => $bookStorageFloorList,
        ]);
    }

    // Fungsi untuk menambahkan buku baru.
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'title'         => 'required|string',
            'author'        => 'required|string',
            'description'   => 'required|string',
            'availability'  => 'required|string',
            'subject_id'    => 'required',
            'floor_id'      => 'required',
            'image_url'     => 'required',
            'photo'         => 'required',
        ]);

        // Kode dari bari 56 hingga 60 digunakan untuk membaca gambar yang diupload, mendeteksi nama file gambar tersebut, dan menyimpan file gambar tersebut ke folder 'public/images' sesuai nama file gambar.
        $filenameWithExt = $request->photo->getClientOriginalName ();// Get Filename
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);// Get just Extension
        $extension = $request->photo->getClientOriginalExtension();// Filename To store
        $fileNameToStore = $filename.'.'.$extension;
        $request->photo->move(public_path('images/'), $fileNameToStore);


        $save = new Book;
        $save->title = $request->input('title');
        $save->author = $request->input('author');
        $save->description = $request->input('description');
        $save->availability = $request->input('availability');
        $save->subject_id = $request->input('subject_id');
        $save->floor_id = $request->input('floor_id');
        $save->image_url = $fileNameToStore;
        $save->save();


        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Buku baru berhasil dibuat!',
        ]);
    }

    // Fungsi untuk mengupdate buku.
    public function update(Book $book, Request $request)
    {
        $data = $this->validate($request, [
            'title'         => 'required|string',
            'author'        => 'required|string',
            'description'   => 'required|string',
            'availability'   => 'required|string',
            'subject_id'    => 'required',
            'floor_id'      => 'required',
            'image_url'     => 'required',
        ]);
        $book->update($data);
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Buku berhasil di-edit!',
        ]);
    }

    // Fungsi untuk menghapus buku.
    public function destroy(Book $book)
    {
        $book->delete();
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Buku berhasil dihapus!',
        ]);
    }
}
