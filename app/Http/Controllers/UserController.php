<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;

class UserController extends Controller
{


        // Fungsi untuk memastikan bahwa route yang ditangani oleh kontroller ini hanya bisa diakses oleh user yang sudah login.
        public function __construct()
    {
        $this->middleware('auth');
    }

    // Fungsi untuk menampilkan daftar anggota/user di tampilan admin.
    public function index(Request $request)
    {
        $data = User::when($request->sort_by, function ($query, $value) {
                $query->orderBy($value, request('order_by', 'asc'));
            })
            ->when(!isset($request->sort_by), function ($query) {
                $query->latest();
            })
            ->when($request->search, function ($query, $value) {
                $query->where('name', 'LIKE', '%'.$value.'%');
            })
            ->paginate($request->page_size ?? 10);
        return Inertia::render('user/index', [
            'items' => $data,
        ]);
    }

    // Fungsi untuk menambahkan anggota/user bary.
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'name'         => 'required|string',
            'member_id'    => 'required|string',
            'email'        => 'required|string',
            'address'      => 'required',
            'phone'        => 'required',
            'role'         => 'required',
            'activated'    => 'required',
        ]);
        User::create($data);
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Anggota baru berhasil dibuat!',
        ]);
    }

    // Fungsi untuk mengupdate data anggota/user.
    public function update(User $user, Request $request)
    {
        $data = $this->validate($request, [
            'name'         => 'required|string',
            'member_id'    => 'required|string',
            'email'        => 'required|string',
            'address'      => 'required',
            'phone'        => 'required',
            'role'         => 'required',
            'activated'    => 'required',
        ]);
        $user->update($data);
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Anggota berhasil di-edit!',
        ]);
    }

    // Fungsi untuk menghapus data anggota/user.
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Anggota berhasil dihapus!',
        ]);
    }


}
