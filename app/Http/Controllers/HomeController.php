<?php

namespace App\Http\Controllers;

use App\Models\BookStorageFloor;
use App\Models\BookSubject;
use App\Models\BookRent;
use App\Models\User;
use App\Models\Book;
use Illuminate\Http\Request;
use Inertia\Inertia;
 
class HomeController extends Controller
{
    // Fungsi untuk memastikan bahwa route yang ditangani oleh kontroller ini hanya bisa diakses oleh user yang sudah login.
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Fungsi untuk menampilkan dashboard admin. Dalam fungsi ini, dilakukan beberapa pengiriman data untuk mempopulasikan statistik singkat yang tersedia di dashboard admin, seperti: jumlah buku terdata, jumlah mahasiswa yang belum diaktivasi, jumlah peminjaman buku yang perlu disetujui, dan sebagainya.
    public function index()
    {

        $b = 0;

        // Menghitung jumlah subjek buku yang terdata di perpustakaan
        $bookSubjectList = BookSubject::get();
        $bookSubjectCount = $bookSubjectList->count();

        // Menghitung jumlah buku terdaftar di perpustakan
        $bookList = Book::get();
        $bookCount = $bookList->count();

        // Menghitung jumlah user terdaftar di perpustakan
        $studentList = User::where('role', 0)->get();
        $studentCount = $studentList->count();

        // Menghitung jumlah mahasiswa yang belum di-aktivasi
        $inactiveStudentList = User::where('role', 0)->where('activated', 'Belum')->get();
        $inactiveStudentCount = $inactiveStudentList->count();

        // Menghitung jumlah peminjaman buku aktif
        $activeBookRentList = BookRent::where('approved', 'Disetujui')->get();
        $activeBookRentCount = $activeBookRentList->count();

        // Menghitung jumlah peminjaman buku yang perlu direview dan disetujui
        $requestedBookRentList = BookRent::where('approved', 'Diajukan')->get();
        $requestedBookRentCount = $requestedBookRentList->count();

        return Inertia::render('home', [
            'bookCount' => $bookCount,
            'bookSubjectCount' => $bookSubjectCount,
            'studentCount' => $studentCount,
            'inactiveStudentCount' => $inactiveStudentCount,
            'activeBookRentCount' => $activeBookRentCount,
            'requestedBookRentCount' => $requestedBookRentCount,
            'b' => $b,
        ]);
    }

    public function index2()
    {
        return Inertia::render('home2');
    }

}
