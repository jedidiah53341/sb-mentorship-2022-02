<?php

namespace App\Http\Controllers;

use App\Models\BookStorageFloor;
use Illuminate\Http\Request;
use Inertia\Inertia;

class BookStorageFloorController extends Controller
{
        // Fungsi untuk memastikan bahwa route yang ditangani oleh kontroller ini hanya bisa diakses oleh user yang sudah login.
        public function __construct()
    {
        $this->middleware('auth');
    }

    // Fungsi untuk menampilkan daftar lantai penyimpanan di tampilan admin.
    public function index(Request $request)
    {
        $data = BookStorageFloor::when($request->sort_by, function ($query, $value) {
                $query->orderBy($value, request('order_by', 'asc'));
            })
            ->when(!isset($request->sort_by), function ($query) {
                $query->latest();
            })
            ->when($request->search, function ($query, $value) {
                $query->where('floor', 'LIKE', '%'.$value.'%');
            })
            ->paginate($request->page_size ?? 10);
        return Inertia::render('bookstoragefloor/index', [
            'items' => $data,
        ]);
    }

    // Fungsi untuk menambahkan data lantai penyimpanan baru.
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'floor'         => 'required',
        ]);
        BookStorageFloor::create($data);
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Buku baru berhasil dibuat!',
        ]);
    }

    // Fungsi untuk mengupdate data lantai penyimpanan.
    public function update(BookStorageFloor $bookstoragefloor, Request $request, $id)
    {
        $data = $this->validate($request, [
            'floor'         => 'required',
        ]);
        $bookstoragefloor->find($id)->update($data);
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Buku berhasil di-edit!',
        ]);
    }

    // Fungsi untuk menghapus data lantai penyimpanan.
    public function destroy(BookStorageFloor $bookstoragefloor, $id)
    {
        $bookstoragefloor->find($id)->delete();
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Buku berhasil dihapus!',
        ]);
    }
}
