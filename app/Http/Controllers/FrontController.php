<?php

namespace App\Http\Controllers;

use App\Models\BookStorageFloor;
use App\Models\BookSubject;
use App\Models\BookRent;
use App\Models\BookRentItem;
use App\Models\User;
use App\Models\Book;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Auth;

class FrontController extends Controller
{

    // Fungsi untuk menampilkan daftar buku dari halaman depan.
    public function front(Request $request)
    {

        // Beberapa variabel ini digunakan untuk menentukan beberapa data, seperti: apakah user sudah login?; apakah user sudah membuat sebuah instansi peminjaman buku?; kalau sudah, berapa id-nya?; dan data lainnya yang akan menentukan antarmuka di halaman depan ini.
        $loggedInUserId = null;
        $loggedInUserName = null;
        $alreadyRequested = null;
        $rent = null;
        $bookRentItems = null;
        $rentId = null;


        if (Auth::check()) {
        $loggedInUserId = Auth::id();
        $loggedInUserName = Auth::user()->name;
        $bookRentRequestedByUser = BookRent::where('user_id', $loggedInUserId)->where('approved', 'Dibuat')->get();

        if (count($bookRentRequestedByUser) > 0) {
            $alreadyRequested = true;
            $rent = $bookRentRequestedByUser;
            $rentId = BookRent::where('user_id', $loggedInUserId)->where('approved', 'Dibuat')->value('id');
            $bookRentItems = BookRentItem::with('book')->where('book_rent_id', $rentId)->paginate($request->page_size ?? 10);
        }
        }
        $data = Book::with('floor')->with('subject')->when($request->sort_by, function ($query, $value) {
                $query->orderBy($value, request('order_by', 'asc'));
            })
            ->when(!isset($request->sort_by), function ($query) {
                $query->latest();
            })
            ->when($request->search, function ($query, $value) {
                $query->where('title', 'LIKE', '%'.$value.'%');
            })
            ->paginate($request->page_size ?? 10);

        return Inertia::render('welcome',[
            'items' => $data,
            'loggedInUserId' => $loggedInUserId,
            'loggedInUserName' => $loggedInUserName,
            'alreadyRequested' => $alreadyRequested,
            'rent' => $rent,
            'rentId' => $rentId,
            'bookRentItems' => $bookRentItems,
        ]);
    }

    // Fungsi untuk membuat instansi peminjaman buku baru.
    public function storeNew(Request $request)
    {
        $data = $this->validate($request, [
            'user_id'      => 'required',
            'rent_since'   => 'required',
            'rent_until'   => 'required',
            'approved'     => 'required',
        ]);
        BookRent::create($data);
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Peminjaman Buku berhasil dibuat!',
        ]);
    }

    // Fungsi untuk menghapus instansi peminjaman buku.
    public function deleteNew($id)
    {   
        $bookrent = BookRent::get();
        $bookrent->find($id)->delete();
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Pengajuan peminjaman buku berhasil dihapus!',
        ]);
    }

    // Fungsi untuk membuat instansi buku yang ingin dipinjam / bookRentItem, bedasarkan id instansi peminjaman buku yang sudah dibuat.
    public function storeNewBookItem(Request $request)
    {   
        $data = $this->validate($request, [
            'book_id'        => 'required',
            'book_rent_id'   => 'required',
        ]);

        BookRentItem::create($data);
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Buku Peminjaman Berhasil Ditambahkan!',
        ]);
    }

    // Fungsi untuk menghapus instansi buku yang ingin dipinjam / bookRentItem, bedasarkan id instansi peminjaman buku yang sudah dibuat.
    public function destroyBookItem(BookRentItem $bookrentitem, $id)
    {   
        $bookrentitem->find($id)->delete();
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Item Pengajuan Berhasil Dihapus!',
        ]);
    }

}
