<?php

namespace App\Http\Controllers;

use App\Models\BookStorageFloor;
use App\Models\BookSubject;
use App\Models\BookRent;
use App\Models\BookRentItem;
use App\Models\Book;
use Illuminate\Http\Request;
use Inertia\Inertia;

class BookRentController extends Controller
{

        // Fungsi untuk memastikan bahwa route yang ditangani oleh kontroller ini hanya bisa diakses oleh user yang sudah login.
        public function __construct()
    {
        $this->middleware('auth');
    }
    
    // Fungsi untuk menampilkan daftar peminjaman buku di tampilan admin.
    public function index(Request $request)
    {
        $data = BookRent::with('user')->when($request->sort_by, function ($query, $value) {
                $query->orderBy($value, request('order_by', 'asc'));
            })
            ->when(!isset($request->sort_by), function ($query) {
                $query->latest();
            })
            ->when($request->search, function ($query, $value) {
                $query->where('rent_since', 'LIKE', '%'.$value.'%');
            })
            ->paginate($request->page_size ?? 10);
        return Inertia::render('book-rent/index', [
            'items' => $data,
        ]);
    }

    // Fungsi untuk menambahkan peminjaman buku baru.
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'user_id'      => 'required',
            'rent_since'   => 'required',
            'rent_until'   => 'required',
            'approved'     => 'required',
        ]);
        BookRent::create($data);
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Peminjaman Buku berhasil dibuat!',
        ]);
    }

    // Fungsi untuk mengupdate peminjaman buku.
    public function update(BookRent $bookrent, Request $request, $id)
    {
        $data = $this->validate($request, [
            'user_id'      => 'required',
            'rent_since'   => 'required',
            'rent_until'   => 'required',
            'approved'     => 'required',
        ]);
        $bookrent->find($id)->update($data);
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Peminjaman Buku berhasil di-edit!',
        ]);
    }

    // Fungsi untuk menghapus peminjaman buku.
    public function destroy(BookRent $bookrent, $id)
    {
        $bookrent->find($id)->delete();
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Peminjaman Buku berhasil dihapus!',
        ]);
    }

    // Fungsi untuk mengajukan peminjaman buku. Fungsi ini dipanggil ketika user melakukan konfirmasi peminjaman buku dari halaman depan, setelah membuat sebuah instansi peminjaman buku dan menambahkan beberapa buku ( instansi bookRentItem ) kedalam peminjaman buku tersebut.
    public function confirm(BookRent $bookrent, $id)
    {
        $bookrent->find($id)->update(['approved' => 'Diajukan']);
        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Peminjaman Buku berhasil dikonfirmasi! Admin akan menghubungi anda apabila pengajuan anda sudah diproses.',
        ]);
    }

    // Fungsi untuk menyetujui peminjaman buku. Fungsi ini dipanggil dengan klik tombol 'Setujui' dari halaman daftar peminjaman buku di dashboard admin.
    public function confirmRent(BookRent $bookrent, $id)
    {
        $bookrent->find($id)->update(['approved' => 'Disetujui']);
        $listOfRentedBookItems = BookRentItem::where('book_rent_id', $id)->get();

        foreach ($listOfRentedBookItems as $bookItem) {
        $book = Book::where('id', $bookItem->book_id)->get();
        $book[0]->update(['availability' => 'Dipinjam']);
        }

        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Peminjaman Buku berhasil dikonfirmasi! Buku sudah boleh diambil dari perpustakaan.',
        ]);
    }

    // Fungsi untuk mengkonfirmasi pengembalian peminjaman buku. Fungsi ini dipanggil dengan klik tombol 'Konfirmasi Pengembalian' dari halaman daftar peminjaman buku di dashboard admin.
    public function confirmFinishedRent(BookRent $bookrent, $id)
    {
        $bookrent->find($id)->update(['approved' => 'Selesai']);
        $listOfRentedBookItems = BookRentItem::where('book_rent_id', $id)->get();

        foreach ($listOfRentedBookItems as $bookItem) {
        $book = Book::where('id', $bookItem->book_id)->get();
        $book[0]->update(['availability' => 'Tersedia']);
        }

        return redirect()->back()->with('message', [
            'type' => 'success',
            'text' => 'Peminjaman Buku berhasil diselesaikan! Pastikan buku yang dipinjam telah ditata di tempatnya.',
        ]);
    }

    // Fungsi untuk menampilkan daftar peminjaman buku di tampilan admin.
    public function showRentedBooks(Request $request, $id)
    {
        $rent_id = $id;
        $data = BookRentItem::with('book')->where('book_rent_id', $id )->when($request->sort_by, function ($query, $value) {
                $query->orderBy($value, request('order_by', 'asc'));
            })
            ->when(!isset($request->sort_by), function ($query) {
                $query->latest();
            })
            ->when($request->search, function ($query, $value) {
                $query->where('book_rent_id', 'LIKE', '%'.$value.'%');
            })
            ->paginate($request->page_size ?? 10);
        return Inertia::render('rentedBooks/index', [
            'items' => $data,
            'rent_id' => $rent_id,
        ]);
    }

}
