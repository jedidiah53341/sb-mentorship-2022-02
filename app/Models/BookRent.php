<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookRent extends Model
{
    use HasFactory;

    // Kode berikut digunakan untuk memampukan pengolahan kolom-kolom di tabel 'book_rent' oleh operasi CRUD.
    protected $fillable = ['user_id','rent_since','rent_until','approved'];

    // Kode berikut menetapkan bahwa setiap satu peminjaman buku dimiliki oleh satu anggota perpustakaan.
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // Kode berikut menetapkan bahwa setiap satu peminjaman buku memiliki banyak item buku yang dipinjam/bookRentItem.
    public function book_rent_item()
    {
        return $this->hasMany(BookRentItem::class);
    }

}
