<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookRentItem extends Model
{
    use HasFactory;

    // Kode berikut digunakan untuk memampukan pengolahan kolom-kolom di tabel 'book_rent_item' oleh operasi CRUD.
    protected $fillable = ['book_id','book_rent_id'];

    // Kode berikut menetapkan bahwa setiap satu item peminjaman buku mewakili id dari sebuah buku/book.
    public function book()
    {
        return $this->belongsTo(Book::class);
    }

    // Kode berikut menetapkan bahwa setiap satu item peminjaman buku mewakili id dari sebuah peminjaman buku/bookRent.
    public function book_rent()
    {
        return $this->belongsTo(BookRent::class);
    }


}
