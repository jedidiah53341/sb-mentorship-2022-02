<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    // Kode berikut digunakan untuk memampukan pengolahan kolom-kolom di tabel 'book' oleh operasi CRUD.
    protected $fillable = ['subject_id','floor_id','title','author','description','availability','image_url'];

    // Kode berikut menetapkan bahwa setiap buku dimiliki oleh satu subjek/kategori buku.
    public function subject()
    {
        return $this->belongsTo(BookSubject::class);
    }

    // Kode berikut menetapkan bahwa setiap buku disimpan pada satu satu lantai penyimpanan buku.
    public function floor()
    {
        return $this->belongsTo(BookStorageFloor::class);
    }

    // Kode berikut menetapkan bahwa setiap buku punya banyak bookRentItem. bookRentItem merepresentasikan sebuah item peminjaman buku. setiap bookRentItem memiliki foreign key ke id satu buku dan foreign key ke id satu peminjaman/bookRent. 
    public function book_rent_item()
    {
        return $this->hasMany(BookRentItem::class);
    }


}
