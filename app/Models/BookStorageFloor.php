<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookStorageFloor extends Model
{
    use HasFactory;

    // Kode berikut digunakan untuk memampukan pengolahan kolom-kolom di tabel 'floor' oleh operasi CRUD.
    protected $fillable = ['floor'];

    // Kode berikut menetapkan bahwa setiap satu data lantai penyimpanan memiliki banyak buku/book yang disimpan di lantai tersebut.
    public function book()
    {
        return $this->hasMany(Book::class,'floor_id');
    }
}
