<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookSubject extends Model
{
    use HasFactory;

    // Kode berikut digunakan untuk memampukan pengolahan kolom-kolom di tabel 'book_subject' oleh operasi CRUD.
    protected $fillable = ['name','description'];
    
    // Kode berikut menetapkan bahwa setiap satu data subjek/kategori buku memiliki banyak buku/book yang termasuk pada subject/kategori tersebut.
    public function book()
    {
        return $this->hasMany(Book::class,'subject_id');
    }

}
