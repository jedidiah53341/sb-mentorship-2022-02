# sb-mentorship-2022-02

Sebuah sistem informasi kepustakaan dengan Laravel, Vue, dan Inertia.js . Situs ini dibuat sebagai pemenuhan penugasan mentorship sanbercode pada periode 7 Feb hingga 28 Feb 2022. 

## Deployment
Situs terdeploy di link heroku berikut : <https://sanber-mentorship-2022-02.herokuapp.com/>

## Penggunaan
#### Mempopulasikan data pada deploy pertama
Ketika situs di-deploy untuk pertama kali-nya, silahkan daftar akun sebagai admin terlebih dahulu. Untuk membuka pilihan radio untuk admin, masukkan angka '12345' ke form input Kunci Admin. Kemudian, harap untuk memasukkan beberapa data ke tabel subjek/kategori buku dan lantai penyimpanan terlebih dahulu, sebab buku tak bisa di-data tanpa ada subjek dan lantai penyimpanan.
#### Mengaktifkan anggota perpustakaan 
Ketika sudah ada subjek, lantai penyimpanan, dan data beberapa buku, maka peminjaman sudah dapat dilakukan. Untuk dapat meminjam, seorang user harus diaktivasi dahulu dengan mengubah kolom Status Aktivasi dari 'Belum' menjadi 'Aktif'. Kolom status aktivasi ada di laman daftar anggota, pada menu edit anggota. Seorang anggota yang baru mendaftar akan selalu memiliki nilai aktivasi awal yaitu 'Belum'.
#### Mengaktifkan anggota perpustakaan 
Ketika seorang pengguna sudah diaktifkan, ia dapat membuat sebuah instansi peminjaman buku dari halaman depan. Setelah membuat instansi, ia dapat menambahkan beberapa buku yang ingin dipinjam. Apabila sudah selesai menambahkan buku, ia selesaikan dengan klik 'Konfirmasi Peminjaman Buku'. Data peminjaman buku dan daftar buku yang dipinjam dalam instansi peminjaman buku tersebut dapat dilihat di tampilan admin, di laman Peminjaman. Pada titik ini, status instansi peminjaman buku tersebut adalah 'Diajukan'. Admin dapat kemudian menyetujui instansi peminjaman tersebut, dan menyebabkan buku-buku yang dipinjam dalam instansi peminjaman tersebut menjadi berstatus 'Dipinjam', sehingga tak dapat ditambahkan oleh instansi peminjaman buku lainnya. Apabila buku sudah dikembalikan, admin dapat klik pada 'Konfirmasi Pengembalian ' untuk mengubah status instansi peminjaman menjadi 'Selesai', dan mengembalikan status buku-buku yang dipinjam dalam instansi peminjaman tersebut menjadi 'Tersedia' kembali.

## Local clone

Clone repo locally

```bash
git clone https://github.com/ahmadfaizk/laravel-inertiajs-vuetify-starterkit
cd laravel-inertiajs-vuetify-starterkit
```

Install PHP Dependencies

```bash
composer install
```

Install NPM Dependencies

```bash
npm install
```

Build assets

```bash
npm run dev
```

Setup Configuration

```bash
cp .env.example .env
```

Generate application key

```bash
php artisan key:generate
```

Run Database migrations

```bash
php artisan migrate
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
