<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookRentItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_rent_items', function (Blueprint $table) {
            $table->id();
            // Kolom book_id dimaksudkan untuk merepresentasikan buku mana yang dipinjam dalam sebuah instansi peminjaman buku/bookRent.
            $table->unsignedBigInteger('book_id');
            // Kolom book_rent_id dimaksudkan untuk merepresentasikan instansi peminjaman buku mana yang melibatkan peminjaman buku dengan id sesuai dengan nilai kolom book_id.
            $table->unsignedBigInteger('book_rent_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_rent_items');
    }
}
