<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            // Member Id merepresentasikan semacam Nomor Induk Mahasiswa.
            $table->string('member_id')->unique();            
            $table->string('email')->unique();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            // Pendaftar dapat memilih dua jenis role, admin ( bernilai 1 ) atau mahasiswa ( bernilai 0 ). Pendaftar hanya dapat membuka radio select untuk pilihan admin apabila memasukan kunci admin yang benar di halaman registrasi.
            $table->string('role');
            // Pendaftar akan memiliki nilai kolom activated 'Belum', yang merepresentasikan bahwa user tersebut baru mendaftar dan perlu diaktifkan dulu melalui halaman daftar keanggotaan yang hanya dapat diakses oleh akun bertipe 'admin'.
            $table->string('activated')->nullable()->default('no');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
