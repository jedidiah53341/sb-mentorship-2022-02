<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('subject_id');
            $table->unsignedBigInteger('floor_id');
            $table->string('title', 100);
            $table->string('author', 100);
            $table->text('description');
            // Nilai dari kolom availability adalah antara 'Tersedia' atau 'Dipinjam'. Ketika buku pertama kali dibuat di halaman daftar buku, maka nilainya akan default ke 'Tersedia'. Nilai tersebut akan diubah menjadi 'Dipinjam' apabila admin menyetujui sebuah peminjaman buku, dan akan kembali menjadi 'Tersedia' apabila admin mengkonfirmasi pengembalian peminjaman buku.
            $table->string('availability', 100);
            // Gambar buku tersimpan di folder 'public/images'
            $table->string('image_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
